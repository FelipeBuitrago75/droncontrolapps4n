# README #

This is the project i constructed for the application to S4N offer job.
 

### What is this repository for? ###

* This project is the admin of a delivery-dron army 

### How do I get set up? ###

* Summary of set up
Fill out a text file with the directions of the drons (it is important to use only [A,I,D] without any spaces or symbols),save the file as IN01.....IN02 and save in location data/in/ ,if everything is right the app must generate files in the location data/out/ 
* Dependencies
In this project were used the following libraries: Lombok ,Junit 5
* How to run tests
Execute the classes into the following package : co.com.s4n.admindron.test
* Deployment instructions
Execute the main method of DronAppMain.java class 


### Who do I talk to? ###

* Andres Felipe Buitrago FelipeBuitrago75@gmail.com