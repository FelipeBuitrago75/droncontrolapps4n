package co.com.s4n.admindron.constants;

public final class ConstantsDronApp {
	/* Paths */
	public static final String PATH_START = "./data/in";
	public static final String PATH_FINISH = "./data/out";
	/* Constants of app */
	public static final int NUMBER_OF_DELIVERIES = 3;
	public static final int NUMBER_OF_DRONES = 20;
	public static final int COVERAGE = 10;
	/* Directions */
	public static final String NORTH = "North";
	public static final String SOUTH = "South";
	public static final String EAST = "East";
	public static final String WEST = "West";
	/* Movements */
	public static final char MOV_A = 'A';
	public static final char MOV_I = 'I';
	public static final char MOV_D = 'D';

	/* Regular Expression */
	public static final String REGEX_STRUCTURE_LINE = "(^[A-D-I]{1,}$)";

	private ConstantsDronApp() {
	}
}
