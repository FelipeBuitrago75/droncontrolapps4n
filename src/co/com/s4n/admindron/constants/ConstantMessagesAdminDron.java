package co.com.s4n.admindron.constants;

public final class ConstantMessagesAdminDron {
	public static final String FINISH_EXECUTION = "Deliveries were successful";
	public static final String ERROR_CREATING_OUTPUT_FILE = "Error creating output file";
	public static final String EXCEED_NUMBER_OF_ORDERS = "The number of orders per drone is exceeded";
	public static final String ERROR_FORMAT_STRUCTURE = "The delivery route format is not correct.";
	public static final String OUT_OF_RANGE = "The delivery is out of range";
	public static final String ERROR_START_READING = "Error: ";
	public static final String IN_THE_FILE = " in the file ";
	public static final String EXISTING_FOLDER = "A folder exists into the start files ";
	public static final String EXCEED_NUMBER_OF_DRONES_AVAILABLE = "The number of files for orders is exceeded";
	public static final String NO_FILE_FOUND = "No file found in the path : ";

	public ConstantMessagesAdminDron() {
	}

}
