package co.com.s4n.admindron.domain;

import lombok.Data;

@Data
public class Location {

	private int x;
	private int y;
	private String orientation;

	public Location(int x, int y, String orientation) {
		this.x = x;
		this.y = y;
		this.orientation = orientation;
	}

}
