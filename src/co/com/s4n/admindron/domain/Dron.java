package co.com.s4n.admindron.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Dron {

	private Location location;
	private List<String> movements;

	public Dron(Location location) {
		this.location = location;
		this.movements = new ArrayList<>();
	}

}
