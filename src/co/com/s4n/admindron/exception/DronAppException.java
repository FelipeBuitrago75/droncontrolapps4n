package co.com.s4n.admindron.exception;

public class DronAppException extends Exception {

	public DronAppException() {
	}

	public DronAppException(String message) {
		super(message);
	}

	public DronAppException(String message, Throwable e) {
		super(message, e);
	}

}
