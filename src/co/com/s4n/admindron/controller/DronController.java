package co.com.s4n.admindron.controller;

import java.util.LinkedHashMap;

import co.com.s4n.admindron.constants.ConstantMessagesAdminDron;
import co.com.s4n.admindron.constants.ConstantsDronApp;
import co.com.s4n.admindron.domain.Dron;
import co.com.s4n.admindron.exception.DronAppException;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DronController {

	private static FileController fileController = new FileController();

	public static String startDeliveries() {
		try {

			LinkedHashMap<String, Dron> listOfDrons = (LinkedHashMap<String, Dron>) fileController
					.obtainRoutes(ConstantsDronApp.NUMBER_OF_DELIVERIES);

			fileController.writeFile(listOfDrons);

		} catch (DronAppException drae) {
			return drae.getMessage();
		}
		return ConstantMessagesAdminDron.FINISH_EXECUTION;
	}

}
