package co.com.s4n.admindron.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import co.com.s4n.admindron.constants.ConstantMessagesAdminDron;
import co.com.s4n.admindron.constants.ConstantsDronApp;
import co.com.s4n.admindron.domain.Dron;
import co.com.s4n.admindron.domain.Location;
import co.com.s4n.admindron.exception.DronAppException;
import co.com.s4n.admindron.util.UtilAdminDron;

public class FileController {

	private static List<String> filesDrones;

	public static List<String> getFilesDrones() {
		return filesDrones;
	}

	public void writeFile(Map<String, Dron> listOfDrones) throws DronAppException {
		for (Map.Entry<String, Dron> entry : listOfDrones.entrySet()) {

			String key = entry.getKey();
			Object value = entry.getValue();

			try (FileWriter outputFile = new FileWriter(
					ConstantsDronApp.PATH_FINISH.concat("/").concat(key.replace("in", "out")));
					PrintWriter pw = new PrintWriter(outputFile)) {
				for (String mov : ((Dron) value).getMovements()) {
					pw.println(mov);
				}

			} catch (Exception e) {
				throw new DronAppException(ConstantMessagesAdminDron.ERROR_CREATING_OUTPUT_FILE);
			}
		}
	}

	public File readFile(String path) throws DronAppException {
		File fileIn = new File(path);
		if (!fileIn.exists()) {
			throw new DronAppException(ConstantMessagesAdminDron.NO_FILE_FOUND + path);
		}
		return fileIn;
	}

	public Map<String, Dron> obtainRoutes(int dronLimit) throws DronAppException {

		listFilePathForFolder();
		LinkedHashMap<String, Dron> listOfDrons = new LinkedHashMap<>();

		for (String fileDron : filesDrones) {
			Dron dron = new Dron(new Location(0, 0, ConstantsDronApp.NORTH));
			String path = ConstantsDronApp.PATH_START.concat("/").concat(fileDron);

			File fileIn = readFile(path);
			try (FileReader fileReader = new FileReader(fileIn);
					BufferedReader buffer = new BufferedReader(fileReader)) {

				String line;

				for (int i = 0; (line = buffer.readLine()) != null; i++) {
					if (!line.equals("")) {
						if (i >= dronLimit) {
							throw new DronAppException(ConstantMessagesAdminDron.EXCEED_NUMBER_OF_ORDERS
									+ ConstantMessagesAdminDron.IN_THE_FILE + path);
						} else if (!UtilAdminDron.validateStructure(line)) {
							throw new DronAppException(ConstantMessagesAdminDron.ERROR_FORMAT_STRUCTURE
									+ ConstantMessagesAdminDron.IN_THE_FILE + path);
						} else if (!UtilAdminDron.validateFullCoverage(line, dron)) {
							throw new DronAppException(ConstantMessagesAdminDron.OUT_OF_RANGE
									+ ConstantMessagesAdminDron.IN_THE_FILE + path);
						}
					}
				}

			} catch (Exception e) {
				throw new DronAppException(ConstantMessagesAdminDron.ERROR_START_READING + e, e);
			}

			listOfDrons.put(fileDron, dron);
		}
		return listOfDrons;
	}

	public static void listFilePathForFolder() throws DronAppException {

		filesDrones = new ArrayList<>();
		File file = new File(ConstantsDronApp.PATH_START);

		if (file.listFiles().length > ConstantsDronApp.NUMBER_OF_DRONES) {
			throw new DronAppException(ConstantMessagesAdminDron.ERROR_START_READING
					+ ConstantMessagesAdminDron.EXCEED_NUMBER_OF_DRONES_AVAILABLE, new DronAppException());

		}
		for (File fileIn : file.listFiles()) {

			if (fileIn.isDirectory()) {
				throw new DronAppException(
						ConstantMessagesAdminDron.ERROR_START_READING + ConstantMessagesAdminDron.EXISTING_FOLDER,
						new DronAppException());
			} else {
				filesDrones.add(fileIn.getName());
			}

		}
	}
}
