package co.com.s4n.admindron.main;

import co.com.s4n.admindron.controller.DronController;

public class DronAppMain {

	public static void main(String[] args) {
		System.out.println(DronController.startDeliveries());
	}

}
