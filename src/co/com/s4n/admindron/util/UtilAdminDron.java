package co.com.s4n.admindron.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.com.s4n.admindron.constants.ConstantsDronApp;
import co.com.s4n.admindron.domain.Dron;
import co.com.s4n.admindron.view.DronView;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UtilAdminDron {

	public static boolean validateStructure(String line) {
		Pattern pattern = Pattern.compile(ConstantsDronApp.REGEX_STRUCTURE_LINE);
		Matcher matcher = pattern.matcher(line);
		if (!matcher.find()) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	public static void generateMoves(Dron dron, char mov) {
		switch (dron.getLocation().getOrientation()) {
		case ConstantsDronApp.NORTH:
			if (mov == ConstantsDronApp.MOV_A) {
				dron.getLocation().setY(dron.getLocation().getY() + 1);
			} else if (mov == ConstantsDronApp.MOV_I) {
				dron.getLocation().setOrientation(ConstantsDronApp.WEST);
			} else {
				dron.getLocation().setOrientation(ConstantsDronApp.EAST);
			}
			break;
		case ConstantsDronApp.SOUTH:
			if (mov == ConstantsDronApp.MOV_A) {
				dron.getLocation().setY(dron.getLocation().getY() - 1);
			} else if (mov == ConstantsDronApp.MOV_I) {
				dron.getLocation().setOrientation(ConstantsDronApp.EAST);
			} else {
				dron.getLocation().setOrientation(ConstantsDronApp.WEST);
			}
			break;
		case ConstantsDronApp.EAST:
			if (mov == ConstantsDronApp.MOV_A) {
				dron.getLocation().setX(dron.getLocation().getX() + 1);
			} else if (mov == ConstantsDronApp.MOV_I) {
				dron.getLocation().setOrientation(ConstantsDronApp.NORTH);
			} else {
				dron.getLocation().setOrientation(ConstantsDronApp.SOUTH);
			}
			break;
		case ConstantsDronApp.WEST:
			if (mov == ConstantsDronApp.MOV_A) {
				dron.getLocation().setX(dron.getLocation().getX() - 1);
			} else if (mov == ConstantsDronApp.MOV_I) {
				dron.getLocation().setOrientation(ConstantsDronApp.SOUTH);
			} else {
				dron.getLocation().setOrientation(ConstantsDronApp.NORTH);
			}
			break;
		default:
			break;
		}

	}

	public static Boolean validateCoverage(Dron dron) {
		return Math.abs(dron.getLocation().getX()) > ConstantsDronApp.COVERAGE
				|| Math.abs(dron.getLocation().getY()) > ConstantsDronApp.COVERAGE ? Boolean.FALSE : Boolean.TRUE;
	}

	public static boolean validateFullCoverage(String line, Dron dron) {
		DronView dronView = new DronView();
		char[] movs = line.toCharArray();
		for (char mov : movs) {
			UtilAdminDron.generateMoves(dron, mov);
		}
		dron.getMovements().add(dronView.printRoute(dron));

		return UtilAdminDron.validateCoverage(dron);
	}

}
