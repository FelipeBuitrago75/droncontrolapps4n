package co.com.s4n.admindron.view;

import co.com.s4n.admindron.domain.Dron;

public class DronView {
	public String printRoute(Dron dron) {
		StringBuilder finalRoute = new StringBuilder("(").append(dron.getLocation().getX()).append(",")
				.append(dron.getLocation().getY()).append(") ").append(dron.getLocation().getOrientation());

		return finalRoute.toString();
	}
}
