package controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import co.com.s4n.admindron.constants.ConstantMessagesAdminDron;
import co.com.s4n.admindron.controller.FileController;
import co.com.s4n.admindron.exception.DronAppException;

public class FileControllerTest {
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	public void listFileForFolder() throws DronAppException {
		FileController.listFilePathForFolder();
		assertTrue(FileController.getFilesDrones() != null);
	}

	@Test
	public void obtainRoutesTestException() {
		FileController fileController = new FileController();

		try {
			fileController.readFile("./datasets");
		} catch (DronAppException e) {
			assertTrue(e.getMessage().contains(ConstantMessagesAdminDron.NO_FILE_FOUND));
		}
	}

}
