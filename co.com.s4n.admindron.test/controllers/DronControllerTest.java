package controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import co.com.s4n.admindron.constants.ConstantMessagesAdminDron;
import co.com.s4n.admindron.controller.DronController;

public class DronControllerTest {
	@Test
	public void beginDeliverysTest() {
		assertEquals(ConstantMessagesAdminDron.FINISH_EXECUTION, DronController.startDeliveries());
	}

}
