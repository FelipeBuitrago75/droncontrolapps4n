package utilTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import co.com.s4n.admindron.constants.ConstantsDronApp;
import co.com.s4n.admindron.domain.Dron;
import co.com.s4n.admindron.domain.Location;
import co.com.s4n.admindron.util.UtilAdminDron;

public class UtilAdmindronTest {

	@Test
	public void validateStructureOfInputCorrect() {
		String path = "IDAID";
		assertTrue(UtilAdminDron.validateStructure(path));
	}

	@Test
	public void validateStructureOfInputIncorrect() {
		String path = "IDxAID";
		assertFalse(UtilAdminDron.validateStructure(path));

		path = "ddaaii";
		assertFalse(UtilAdminDron.validateStructure(path));
	}

	@Test
	public void validateFullCoverageCorrect() {
		assertTrue(UtilAdminDron.validateFullCoverage("AAIIDD", new Dron(new Location(0, 0, ConstantsDronApp.NORTH))));

	}

	@Test
	public void validateFullCoverageIncorrect() {
		assertFalse(UtilAdminDron.validateFullCoverage("AAAAAAAAAAAAAA",
				new Dron(new Location(0, 0, ConstantsDronApp.NORTH))));

	}

	@Test
	public void validateGenerateMoves() {
		Dron dron = new Dron(new Location(0, 0, ConstantsDronApp.NORTH));
		UtilAdminDron.generateMoves(dron, ConstantsDronApp.MOV_A);
		assertEquals(ConstantsDronApp.NORTH, dron.getLocation().getOrientation());
		assertEquals(1, dron.getLocation().getY());
		assertEquals(0, dron.getLocation().getX());
	}

	@Test
	public void validateGenerateMovesI() {
		Dron dron = new Dron(new Location(0, 0, ConstantsDronApp.NORTH));
		UtilAdminDron.generateMoves(dron, ConstantsDronApp.MOV_I);
		assertEquals(ConstantsDronApp.WEST, dron.getLocation().getOrientation());
		assertEquals(0, dron.getLocation().getY());
		assertEquals(0, dron.getLocation().getX());
	}

	@Test
	public void validateGenerateMovesD() {
		Dron dron = new Dron(new Location(0, 0, ConstantsDronApp.NORTH));
		UtilAdminDron.generateMoves(dron, ConstantsDronApp.MOV_D);
		assertEquals(ConstantsDronApp.EAST, dron.getLocation().getOrientation());
		assertEquals(0, dron.getLocation().getY());
		assertEquals(0, dron.getLocation().getX());
	}
}
